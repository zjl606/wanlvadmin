import axios from 'axios';
import qs from 'qs';
import config from '../config';

const request = axios.create({
  baseURL: config.dev.BASE_URL, 
  timeout: 5000, // request timeout
  transformRequest: [function(data) {
    return qs.stringify(data)
  }]
})

// // 添加请求拦截器
// request.interceptors.request.use(function (config) {
//     // 在发送请求之前做些什么
//     console.log('config:::', config);
//     return config;
//   }, function (error) {
//     // 对请求错误做些什么
//     return Promise.reject(error);
//   });

// // 添加响应拦截器
// request.interceptors.response.use(function (response) {
//     // 对响应数据做点什么
//     console.log('response:::', response);
//     return response;
//   }, function (error) {
//     // 对响应错误做点什么
//     return Promise.reject(error);
//   });

export default request;