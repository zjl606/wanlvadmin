import Vue from 'vue'
// 设置全局变量
import global_ from './components/Global.vue'
import App from './App.vue'
import router from './router';
import store from './store';
import axios from 'axios';
// axios.defaults.withCredentials=true;
import VueQuillEditor from 'vue-quill-editor'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './css/reset.css';
import './css/unit.css';
import './css/skin.css';
import './css/function.css';
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.prototype.GLOBAL = global_
Vue.prototype.$axios = axios;

Vue.use(ElementUI)
Vue.use(VueQuillEditor, /* { default global options } */)
          
Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
