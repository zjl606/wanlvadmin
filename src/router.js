import Vue from 'vue';
import Router from 'vue-router';
import Home from './components/Home.vue'
import Login from './views/Login.vue';
import UserManage from './views/UserManage.vue';
import IssueManage from './views/IssueManage.vue';
import BiddingManage from './views/BiddingManage.vue';
import CaseManage from './views/CaseManage.vue';
import ExampleManage from './views/example/ExampleManage';
import AddExample from './views/example/AddExample';
import PlayletManage from './views/playlet/PlayletManage.vue';
import AddPlaylet from './views/playlet/AddPlaylet';
import LegalKnowledgeManage from './views/knowledge/LegalKnowledgeManage';
import AddKnowledge from './views/knowledge/AddKnowledge';
import AddKnowledgeClassify from './views/knowledge/AddKnowledgeClassify';
import LawyerAuditManage from './views/LawyerAuditManage'
import LawyerManage from './views/LawyerManage'
import PublicWelfareManage from './views/PublicWelfareManage';
import NoticeManage from './views/notice/NoticeManage';
import AddNotice from './views/notice/AddNotice';
import BannerManage from './views/ad/BannerManage';
import AddAd from './views/ad/AddAd';
import ConsultManage from './views/consult/ConsultManage';
import AddConsultRecord from './views/consult/AddConsultRecord';
import ClassifyManage from './views/classify/ClassifyManage';
import AddClassify from './views/classify/AddClassify';
import TeamManage from './views/team/TeamManage';
import AddLawyer from './views/team/AddLawyer';
import TradeRecordManage from './views/TradeRecordManage';
import WithdrawCashManage from './views/withdrawCash/WithdrawCashManage';
import WithdrawCashSetting from './views/withdrawCash/WithdrawCashSetting';
import OrganizationalStructureManage from './views/organizationalStructure/OrganizationalStructureManage';
import AddStaff from './views/organizationalStructure/AddStaff';
import PermissionManage from './views/permission/PermissionManage';
import PermissionSettings from './views/permission/PermissionSettings';
// import global_ from './components/Global'
import store from './store/index';

Vue.use(Router);

const router = new Router({
    mode: 'history',
    // base: PerformanceObserverEntryList.env.BASE_URL,
    routes: [
      {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
          requireAuth: true,
        }
      },
      {
        path: '/home',
        name: 'Home',
        component: Home,
        meta: {
          requireAuth: true,
        },
        children: [
          // UserHome will be rendered inside User's <router-view>
          // when /user/:id is matched
          {
            path: '', 
            component: UserManage,
            meta: {
              requireAuth: true,
            },
          },
          
          // UserProfile will be rendered inside User's <router-view>
          // when /user/:id/profile is matched
          // {
          //   path: 'user', 
          //   component: UserManage,
          //   meta: {
          //     requireAuth: true,
          //   },
          // },
          {
            path: 'issue', 
            component: IssueManage,
            meta: {
              requireAuth: true,
            },
          },
          {
            path: 'bidding', 
            component: BiddingManage,
            meta: {
              requireAuth: true,
            },
          },
          {
            path: 'case', 
            component: CaseManage,
            meta: {
              requireAuth: true,
            },
          },
          {
            path: 'example',
            component: ExampleManage,
            meta: {

            }
          },
          {
            path: 'addExample',
            component: AddExample
          },
          {
            path: 'playlet', 
            component: PlayletManage,
            meta: {
              requireAuth: true,
            },
          },
          {
            path: 'addPlaylet',
            component: AddPlaylet
          },
          {
            path: 'legalKnowledge',
            component: LegalKnowledgeManage,
            meta: {

            }
          },
          {
            path: 'addKnowledge',
            component: AddKnowledge
          },
          {
            path: 'addKnowledgeClassify',
            component: AddKnowledgeClassify
          },
          {
            path: 'lawyerAudit',
            component: LawyerAuditManage,
            meta: {

            }
          },
          {
            path: 'lawyer',
            component: LawyerManage,
            meta: {

            }
          },
          {
            path: 'publicWelfare',
            component: PublicWelfareManage,
            meta: {

            }
          },
          {
            path: 'notice',
            component: NoticeManage,
            meta: {

            }
          },
          {
            path: 'addNotice',
            component: AddNotice
          },
          {
            path: 'banner',
            component: BannerManage,
            meta: {

            }
          },
          {
            path: 'addAd',
            component: AddAd
          },
          {
            path: 'consult',
            component: ConsultManage,
            meta: {

            }
          },
          {
            path: 'addConsultRecord',
            component: AddConsultRecord
          },
          {
            path: 'classify',
            component: ClassifyManage,
            meta: {

            }
          },
          {
            path: 'addClassify',
            component: AddClassify
          },
          {
            path: 'team',
            component: TeamManage,
            meta: {

            }
          },
          {
            path: 'addLawyer',
            component: AddLawyer
          },
          {
            path: 'tradeRecord',
            component: TradeRecordManage,
            meta: {

            }
          },
          {
            path: 'withdrawCash',
            component: WithdrawCashManage,
            meta: {

            }
          },
          {
            path: 'withdrawCashSetting',
            component: WithdrawCashSetting
          },
          {
            path: 'organizationalStructure',
            component: OrganizationalStructureManage,
            meta: {

            }
          },
          {
            path: 'addStaff',
            component: AddStaff
          },
          {
            path: 'permission',
            component: PermissionManage,
            meta: {

            }
          },
          {
            path: 'permissionSettings',
            component: PermissionSettings
          }
        ]
      },
      {
        path: '/login',
        name: 'Login',
        component: Login,
        meta: {
          requireAuth: false,
        },
      }
    ]
});

router.beforeEach((to, from, next) => {
  // ...
  console.log('to:::', to);
  console.log('from:::', from);
  if(to.path == '/login') {
    next();
  } else {
    // const token = global_.getToken();
    console.log('router::store:::', Vue.$store);
    console.log('store::--roter:', store.state.user.token);
    if(store.state.user.token) {
      next();
    } else {
      next({ path: '/login' })
    }
    // next();
  }
  
})

export default router;