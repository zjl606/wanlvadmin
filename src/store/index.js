import Vue from 'vue'
import Vuex from 'vuex';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: null,
    count: 0,
  },
  mutations: {
    setToken(state, token) {
      state.token = token;
    },
    increment (state) {
      state.count++
    }
  },
  actions: {

  },
  modules: {
    user,
  }
});