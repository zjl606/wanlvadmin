import request from '../../utils/request';
// import qs from 'qs';
const user = {
  state: {
    token: null,
    user: '88988'
  },

  getters: {
    user: state => state.user,  
  },

  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;  
    }
  },

  actions: {
    // 用户名登录
    LOGIN({ commit }, userInfo) {

      return new Promise((resolve, reject) => {
        request.post('/api/admin/login', userInfo).then(res => {
          console.log('res::', res);
          commit('SET_TOKEN', res.data.token);
          resolve(res);
        }).catch(err => {
          console.error('err::::', err);  
          reject(err);
        })
      });
      
    },

  }
}

export default user